package br.com.vertechit.example.aws.s3;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

public class GetObject {
	private static String bucketName = "bucketName";
	private static String key = "key.ext";

	public static void main(String[] args) throws IOException {
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.SA_EAST_1).build();

		try {
			System.out.println("Downloading an object");
			
			GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, key);
			S3Object         s3object         = s3Client.getObject(getObjectRequest);
			
			System.out.println("Content-Type: " + s3object.getObjectMetadata().getContentType());
			
			InputStream input = s3object.getObjectContent();
			
			File targetFile = new File("src/main/resources/targetFile.ext");
			 
		    Files.copy(
		      input, 
		      targetFile.toPath(), 
		      StandardCopyOption.REPLACE_EXISTING);
		 
		    input.close();
			
			System.out.println("Ok");

		} catch (AmazonServiceException ase) {
			System.out.println("Caught an AmazonServiceException, which" + " means your request made it " + "to Amazon S3, but was rejected with an error response" + " for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out.println("Caught an AmazonClientException, which means" + " the client encountered " + "an internal error while trying to " + "communicate with S3, " + "such as not being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
	}
}